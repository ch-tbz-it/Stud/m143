![TBZ Logo](../x_git/tbz_logo.png)
![m143 Picto](../x_git/M143_Picto.png)


[TOC]

# LB2 Projektarbeit 

Bei der zweiten Leistungsbeurteilung handelt es sich um ein Team-Projekt (vorzugsweise 2-3 LN), welches direkt im Anschluss an die LB1 gestartet wird. Dauer 2 Wo mit 11 Lektionen.

### Auftrag
Realisierung einer Speicherplattform für heterogene IT-Umgebungen. 
Dieser Auftrag baut auf den bisher erworbenen Kenntnissen auf. 
Für die erfolgreiche Firma **Yeez1 AG** wird gemäss Anforderungskatalog eine neue On-Premise Backup-Infrastruktur konzipiert und virtuell in Betrieb genommen (Virtuelle Machbarkeitsstudie).

![(Vorgehen.png)](Vorgehen.png)

Folgende Kompetenzen werden erarbeitet / vertieft:

- Kann Lösungsvarianten evaluieren, entwickeln und bereitstellen
- Kann Betriebs- und Wartungsdokumente entwickeln und nachführen.
- Kann Backup- und Restore-Systeme für den produktiven Betrieb freigeben.

---

[**LB2 Gesamtauftrag**](LB2_v2.0.docx)

---
**Ressourcen Bilder / Drawio / Tipps LB2**
Siehe Dateien oben in der [Dateiliste](.)

**Ressourcen Theorie iSCSI**

1. [Thomas Kern](https://www.thomas-krenn.com/de/wiki/ISCSI_Grundlagen)
2. [Guide to iSCSI](https://ascentoptics.com/blog/de/a-comprehensive-guide-to-iscsi-understanding-how-it-works-and-its-benefits/)
3. [Was ist iSCSI](https://www.storage-insider.de/was-ist-iscsi-a-679345/)

   Siehe auch ZFS, RAID Z ...

**Ressourcen Installation**

- [Download TrueNAS Core](https://www.truenas.com/download-truenas-core/)
- [TrueNAS Core 12 Install and Basic Setup](https://www.youtube.com/watch?v=WjLaK8yQAag)


- [How to Install TrueNAS on **VMware**](https://www.youtube.com/watch?v=ft0eOCkcTEg) (Youtube 5' 20")
- [How to install TrueNAS on **Virtual Box**](https://www.youtube.com/watch?v=RLg1mWy5v7s) (Youtube 16' 06")
- [How to install TrueNAS on **Virtual Box**](https://www.youtube.com/watch?v=y-pg_wO8zOs) (Youtube 11' 29")
- [How to create an **iSCSI** Target with TrueNAS](https://www.youtube.com/watch?v=JzX6c58ydY4) (Youtube 2' 56")
- [How to configure **iSCSI** (Block share) in TrueNAS](https://www.youtube.com/watch?v=meJw1ruLpcs) (Youtube 15' 16")


**Ressourcen Konfiguration**

- [Setting up Windows iSCSI Block ](https://www.truenas.com/blog/iscsi-shares-on-truenas-freenas/) (7' 24")
- [Adding SMB Shares](https://www.truenas.com/docs/scale/scaletutorials/shares/smb/addsmbshares/) (1' 30")
- [NFS Share Creation](https://www.truenas.com/docs/core/coretutorials/sharing/nfs/nfsshare/) (Doku)
