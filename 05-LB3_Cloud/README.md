![TBZ Logo](../x_git/tbz_logo.png)
![m143 Picto](../x_git/M143_Picto.png)


[TOC]

---

# LB3 Projektarbeit 

Bei der dritten Leistungsbeurteilung handelt es sich nochmals um ein Team-Projekt, welches direkt im Anschluss an die LB2 gestartet wird. Dauer 1-2 Wo mit max. 11 Lektionen.

### Auftrag
Realisierung einer Speicherplattform für heterogene IT-Umgebungen. 
Dieser Auftrag baut auf den bisher erworbenen Kenntnissen auf. 
Für die erfolgreiche Firma **Yeez1 AG** wird gemäss Anforderungskatalog eine neue Cloud Backup-Infrastruktur konzeptionell definiert und verifiziert (Proof of Concept).

![(Vorgehen)](Vorgehen.png)

Folgende Kompetenzen werden erarbeitet / vertieft:

- Kann Lösungsvarianten evaluieren, entwickeln und bereitstellen
- Kann Betriebs- und Wartungsdokumente entwickeln und nachführen.
- Kann Backup- und Restore-Systeme für den produktiven Betrieb freigeben.


[**LB3 Gesamtauftrag**](LB3_v4.0.docx)


---

**Ressourcen**

* [Reise in den Datenspeicher](https://www.storage-insider.de/die-reise-des-datenspeichers-in-die-cloud-a-984250/?cmp=nl-35&uuid=D70ECB43-FB59-415D-8814-16CC27E43229)
* [Doku, PDFs](Doku)
* [CloudBerry Explorer / MSP 360](https://www.msp360.com/explorer/windows/)
* Cloud Anbieter
* Open Source Software
* Backup Software

