![TBZ Logo](../x_git/tbz_logo.png)
![m143 Picto](../x_git/M143_Picto.png)


[TOC]

---

# Datensicherungskonzept

![Konzeptionierung](x_git/Backupkonzept.png)

Ziel eines Datensicherungskonzeptes ist es, klar und verständlich zu dokumentieren, welche Maßnahmen IT-Systeme und Daten vor versehentlichem Verlust schützen und wie im Verlustfall Daten schnell rekonstruiert werden können.

Um diese Ziele erreichen zu können, muss/sollte das Datensicherungskonzept folgende Punkte berücksichtigen:

- Verantwortliche Personen
- IT-Systeme (Server, Clients, ggf. mobile Geräte)
- Datenspeicherarten und Formate (Datenbanken, Files oder auch Festplatten und entsprechende Datenformate)
- Betroffene Daten und Datenarten (Personaldaten, Kundendaten etc.)
- Art der Datensicherung (Vollsicherung, Inkrementell oder Differenziell)
- Häufigkeit der Sicherungen (täglich, wöchentlich, jährlich)
- Kombinationen aus Art und Häufigkeit
- Verwendetes RAID im Backup-Server
- Sicherungen der Backup Server: Bandsicherungen, Co-Location, etc.
- Sicherungsmaßnahmen für den Backup-Server    
     - Räumlichkeiten (Zugang, Gefahr von Brand- und Wasserschäden)
     - Hardware Monitoring (Speicherkapazität, Temperatur, allgemein Zustand, Benachrichtigung über fehlgeschlagene Backups)
     - Zutritts- und Zugangsberechtigungen
     - Verschlüsselung der Datenträger des Backup-Servers 
- Prozesse zum Zurückspielen/Wiederherstellen von Daten (ggf. in Kombination zum Notfallhandbuch)
- Vorgaben zur Nutzung der Daten für andere Zwecke (Beweissicherung, Abgleich von Versionsständen, Erfüllung von Nachweispflichten etc.)

Neben der Erstellung des Datensicherungskonzeptes ist es zudem essentiell, die Datensicherungen testweise zurück ins System zu spielen. So können sowohl die Funktionstüchtigkeit der Sicherungen geprüft, als auch die grundsätzliche Vorgehensweise für den Notfall erprobt werden. Solche Testläufe sollten in regelmäßigen Abständen erfolgen und sind im Datensicherungskonzept zu beschreiben.

## Arbeitsaufträge / Unterlagen

* 01 - [Einflussfaktoren](01-Einfuehrung/Einfuehrung_Datensicherungskonzept_inkl_Textfelder_v1.5.pdf) (Auftrag, Textfelder im PDF ausfüllen) 
* 02 - [Erhebung](02-Erhebung/Backup-Konzept_On-Premise_v2.4.pdf) (Auftrag, [Erhebungsvorlage](02-Erhebung/Backup-Konzept_On-Premise_Vorlage.xlsx) **herunterladen** und ausfüllen)
* 03 - [Planung (Benotet 10%)](03-Planung/Datensicherung_v2.0.pdf) (Auftrag Lernprodukt: Konzept-Dokumentation) 


### Ergänzung Backup-Level-Art

![Konzeptionierung](x_git/Backup-Level_Art.png)

<br>

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m143/-/tree/main)


---