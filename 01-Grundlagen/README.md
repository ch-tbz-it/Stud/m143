![TBZ Logo](../x_git/tbz_logo.png)
![m143 Picto](../x_git/M143_Picto.png)

[TOC]

# Grundlagen

Firmen, aber auch Privatpersonen, speichern immer mehr Daten und sind gleichzeitig immer stärker auf sie angewiesen.
Gehen Daten verloren, z. B. durch defekte Hardware, Malware oder versehentliches Löschen, können
gravierende Schäden entstehen. Dies kann sowohl klassische IT-Systeme, wie Server, Clients oder auch Dateiablagen und Datenbanken
betreffen. Aber auch Router, Switches oder IoT-Geräte können schützenswerte Informationen, wie z.B.
Konfigurationen, speichern. Deswegen umfasst der Begriff IT-System im Rahmen dieses Moduls alle
möglichen Arten und Formen von IT-Komponenten, die schützenswerte Informationen speichern.
Durch **regelmäßige Datensicherungen** lassen sich Auswirkungen von Datenverlusten jedoch
**minimieren**. Eine Datensicherung soll gewährleisten, dass durch einen redundanten Datenbestand der
Betrieb der Informationstechnik kurzfristig (oder gemäss vertraglicher Vereinbarung) wieder aufgenommen werden kann, wenn Teile des
operativen Datenbestandes verloren gehen. 

Die unten aufgeführten Arbeitsaufträge sind didaktisch so aufgebaut, dass sie der Reihe nach durchgeführt werden sollten. Die dafür vorgesehene Zeit umfasst 12 - 14 Lektionen. Sämtliche Inhalte sind auf die LB1 ausgerichtet, die im Anschluss als [schriftliche Einzelarbeit](../07-Leistungsbeurteilungen/LB1) durchgeführt werden kann. 

## Arbeitsaufträge / Unterlagen

* 01 - [Moduleinstieg](01-Einstieg/01_Moduleinstieg_V1.5.pdf) (PPT &rarr; PDF für LN) 
* 02 - [Thesen](02-Thesen/02_Moduleinstieg-These-Antithese.md) - Arbeitsauftrag 
* 03 - [Weshalb schütze ich meine Daten](03-Weshalb-Datenschutz/Weshalb_schuetze_ich_meine_Daten_v2.1.pdf) (PDF für LN &rarr; Mindmap)
* 04 - [Fachbegriffe](04-Fachbegriffe) (Ordner mit Teilaufträgen &rarr; Gruppenpuzzle)
     * [A Backup SW](04-Fachbegriffe/A_Arbeitsauftrag_Vertiefung_Fachbegriffe_Backup-SW_V1.1.pdf)
     * [B Infrastruktur: DAS NAS SAN](04-Fachbegriffe/B_Arbeitsauftrag_Vertiefung_Fachbegriffe_DAS_NAS_SAN_v1.0.pdf)
     * [C Anbindung: FC iSCSI NVMe-oF](04-Fachbegriffe/C_Arbeitsauftrag_Vertiefung_Fachbegriffe_FC_iSCSI_NVMe-oF_V1.0.pdf)
     * [D Vertiefung Restore vs. Disaster Recovery](04-Fachbegriffe/D_Arbeitsauftrag_Vertiefung_Fachbegriffe_Restore_vs_DR_V1.3.pdf)
     * [E Schnittstellen: SATA SAS PCIe](04-Fachbegriffe/E_Arbeitsauftrag_Vertiefung_Fachbegriffe_SATA_SAS_PCIe_V1.0.pdf)

* 05 - [Speichermedien](05-Speichermedien/README.md) (LP stellt DOCX-Datei im Teams-Kanal zur Verfügung &rarr; DOCX-Datei online bearbeiten!)


<br>

---

### Ergänzende Ressourcen

- ![Video](../x_git/Video.png)[Backup-Infrastruktur Bank Bär erklärt](https://tbzedu.sharepoint.com/sites/IT_PE23a/_layouts/15/stream.aspx?id=%2Fsites%2FIT%5FPE23a%2FFreigegebene%20Dokumente%2FGeneral%2FBackup%5FInfrastruktur%5FEnterprise%2Emp4&referrer=StreamWebApp%2EWeb&referrerScenario=AddressBarCopied%2Eview%2E07981f1d%2D273e%2D4ae7%2D91d2%2Da935491fe250&isDarkMode=false) ![Bank Bär](04-Fachbegriffe/B_Backup_Infrastruktur_Enterprise.jpg)
- [Stärkung des Datenschutzes](https://www.admin.ch/gov/de/start/dokumentation/medienmitteilungen.msg-id-90134.html) (Datenschutzrecht ab 1.September 2023)  
<br>

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m143/-/tree/main)


---