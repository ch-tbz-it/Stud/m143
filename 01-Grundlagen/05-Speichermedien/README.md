[10]: https://git-scm.com/downloads
[32]: https://man.openbsd.org/sshd_config


# Speichermedien


Hier ein Überblick:
![Massenspeicher](Data_Massenspeicher.pdf)

Ein Blick in unser TBZ-Museum lohnt sich:
![Museum](Speichermedien.jpg)


## Arbeitsauftrag


* Die LP weisst ihnen eine Massenspeicher-Art in folgendem Dokument zu: [Übersicht (PDF)](Uebersicht_Speichermedien_v1.6.pdf)
* Die LP stellt paralle eine **DOCX-Datei im Teams-Kanal** zur Verfügung &rarr; DOCX-Datei online bearbeiten!
* Allenfalls als Hausaufgabe fertig stellen
* Ergänzende Ressourcen: Sie PDFs im Verzeichnis oben


---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/TE/m143)


---