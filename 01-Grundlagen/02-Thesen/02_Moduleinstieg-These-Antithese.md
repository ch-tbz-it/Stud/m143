**Arbeitsauftrag:** Begründen Sie, weshalb die untenstehenden Aussagen stimmen
oder falsch sind. Verwenden Sie für Ihre Begründung möglichst Beispiele, die
einen Praxisbezug haben.

**Sozialform:** Partnerarbeit, jedem Team wird eine These zugewiesen.

**Dauer:** 10 min für Auftragsbearbeitung mit anschliessender Diskussionsrunde
im Plenum.

**Zielsetzung:** Unterrichtseinstig in das Modul 143. Die Lernenden sollen sich
kritisch mit den Thesen auseinandersetzen und möglichst mit ihrem Vorwissen
Antithesen bilden.

| 1  | Daten-Backup kostet nichts.                                                                       |
|----|---------------------------------------------------------------------------------------------------|
| 2  | Backups sind heute nicht mehr erforderlich.                                                       |
| 3  | Daten in der Cloud sind immer sicher.                                                             |
| 4  | Nach dem Einrichten eines Backups ist sichergestellt, dass alle Daten immer gesichert werden.     |
| 5  | Der Datenverlust in einer Firma kostet nichts.                                                    |
| 6  | Es müssen immer alle Daten gesichert werden.                                                      |
| 7  | Eine Datensicherung pro Woche genügt.                                                             |
| 8  | Es gibt nur eine Art von Daten. Deshalb kann die Backuperstellung immer gleich ausgeführt werden. |
| 9  | Der Faktor Zeit spielt bei der Backuperstellung keine Rolle.                                      |
| 10 | Google macht von den Benutzerdaten keine Backups.                                                 |
| 11 | Das Erstellen von Backups ist im Geschäftsumfeld freiwillig.                                      |
