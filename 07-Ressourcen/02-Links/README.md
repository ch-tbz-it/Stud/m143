[10]: https://git-scm.com/downloads
[32]: https://man.openbsd.org/sshd_config


Links
====

<br>

**Backup-Konzept:**
* [Backup-Konzept und IT-Notfallhandbuch, oder warum Backup nicht gleich Backup ist](https://storageconsortium.de/content/content/backup-konzept-und-it-notfallhandbuch-oder-warum-backup-nicht-gleich-backup-ist)
* [Backup-Konzept - Wie Sie die Risiken von Datenverlust minimieren](https://www.ines-it.de/news/backup-konzept-wie-sie-die-risiken-von-datenverlust-richtig-minimieren/)


---

**ePortfolio Luis Lüscher:**
* [M143: Backup- und Restore-Systeme implementieren](https://luis-luescher.com/informatik/modularbeiten/m143/)

<br>

---

> [⇧ **Zurück zu Ressourcen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/TE/m143)

---