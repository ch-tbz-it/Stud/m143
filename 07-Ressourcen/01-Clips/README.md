[10]: https://git-scm.com/downloads
[32]: https://man.openbsd.org/sshd_config


Clips
====

**Datacenters:**
* [Inside a Google-Datacenter](https://www.youtube.com/watch?v=XZmGGAbHqa0) (5' 27" Quelle Youtube)
* [Inside Amazon's Massive Data Center](https://www.youtube.com/watch?v=q6WlzHLxNKI) (9' 31" Quelle Youtube)

**Methoden, Strategien:**
* [Datensicherung für Unternehmen: Die 3-2-1 Methode für Backups](https://www.youtube.com/watch?v=oUgkVzy02gQ) (6' 04" Quelle Youtube)
* [Englisch: 4 elements to good backup DR strategy and why testing backups is essential](https://www.youtube.com/watch?v=0UrtpAALwKQ) (5' 22" Quelle Youtube)


**Backuparten:**
* [Backuparten](https://www.youtube.com/watch?v=oUgkVzy02gQ) (15' 05" Quelle Youtube)
* [Englisch: Incremental vs Differential Backup, & Full - Explained](https://www.youtube.com/watch?v=o-83E6levzM) (6' 55" Quelle Youtube)

**Wechselschema:**

* [Die richtige Backup Strategie - Das GVS oder Generationen Prinzip](https://www.youtube.com/watch?v=aH815nTlW28) (7' 54" Quelle Youtube)
* [Englisch: Grandfather Father Son Backups](https://www.youtube.com/watch?v=H8jOdWynOhE) (2' 00" Quelle Youtube)

**Archivierung:**
* [Wie funktioniert digitale Archivierung](https://www.youtube.com/watch?v=gSvrVybdw_8) (3' 33" Quelle Youtube)


**Disaster Recovery:**
* [Bei Katastrophen: Das Disaster Recovery Management der Telekom](https://www.youtube.com/watch?v=SOv1UyHWAZU) (9' 50" Quelle Youtube)
* [Englisch: Disaster Recovery vs. Backup: What's the difference?](https://www.youtube.com/watch?v=07EHsPuKXc0) (9' 31" Quelle Youtube)

**High Availability:**
* [Englisch: High Availability Architectures](https://www.youtube.com/watch?v=lJLk3yPRV1w) (4' 38" Quelle Youtube)


**Datensicherung im privaten Bereich:**
* [Backup und Datensicherung für Fotografen und Filmemacher!](https://www.youtube.com/watch?v=8jXyIJrbKH4) (5' 23" Quelle Youtube)

<br>

--- 

**LB2: Backup- und Restoreplattform**
* [How to Install TrueNAS on VMware](hhttps://www.youtube.com/watch?v=A_iIkqqQ-Zw) (5' 27" Quelle Youtube)



<br>

---

> [⇧ **Zurück zu Ressourcen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/TE/m143)

---