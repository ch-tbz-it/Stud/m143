[10]: https://git-scm.com/downloads
[32]: https://man.openbsd.org/sshd_config


Literatur
====

<br>

**Cloud:**
* [Cloud Monitor 2021](Cloud/cloudmonitor2021_SDC_v14_BF.pdf)
* [c't Artikel: Cloudstorage: Daten langfristig in der Cloud speichern](Cloud/Cloudstorage_%20Daten%20langfristig%20in%20der%20Cloud%20sichern.pdf)

**Datensicherheit:**
* [Backup-Archivierung: Gestern, Heute, Morgen](Datensicherheit/backup-archivierung--gestern-heute-und-morgen.pdf)
* [Storage Insider: Best of 2022](Datensicherheit/best-of-2022-storage-insider.pdf)
* [CIO Briefing: Das aktuelle Technology-Update für IT-Manager](Datensicherheit/ciobriefing-032022.pdf)
* [Wie man Daten vor Krypto-Erpressung schützt](Datensicherheit/Wie_man_seine_Daten_vor_Krypto-Erpressung_schuetzt.pdf)
* [Wie die 6 grössten Probleme der Datenspeicherung gelöst werden](Datensicherheit/wie-sie-die-6-groessten-probleme-der-datenspeicherung-loesen.pdf)

**Langzeitarchivierung:**
* [Digitale Daten langfristig zuverlässig speichern](Langzeitarchivierung/Digitale%20Daten%20langfristig%20zuverl%C3%A4ssig%20speichern.pdf)
* [Datensicherung und Langzeitarchivierung mit Festplatten](Langzeitarchivierung/Digitale%20Datensicherung_Langzeitarchivierung_mit_Festplatten.pdf)
* [M-Disc als Speichermedium nutzen](Langzeitarchivierung/Langzeitarchivierung_%20M-Disc_als_Speichermedium_nutzen.pdf)

<br>

---

> [⇧ **Zurück zu Ressourcen**](../README.md)

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/TE/m143)

---