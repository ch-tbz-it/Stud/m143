![TBZ Logo](./x_git/tbz_logo.png)
![m143 Picto](./x_git/M143_Picto.png)


[TOC]

---

# M143 - Backup- und Restore-Systeme implementieren

Dieses Repository enthält Ressourcen für die Lernenden zur Durchführung des Moduls M143 an der TBZ. Es basiert auf den Vorgaben der BIVO2021.

**Ups ...**

Kann jedem passieren:

![](x_git/Burned_Laptop.jpg)

Infrastruktur im Keller kann feucht werden:

![](x_git/Wassereinbruch_Serverraum.png)

Cloudbrand 2014:

![](x_git/HVO-Brand.jpg)



---


# SOL Unterricht

[**&rarr; Kompetenzmatrix**](06-Kompetenzmatrix/README.md)

[**&rarr; Erklärung**](06-Kompetenzmatrix/LB-Punktebewertung-Praxisarbeit-mit_Erklärungen.pdf)
   

---

# Kassischer Unterricht

Hier finden Sie einen Poster, der ihnen einen Überblick und viele Details liefert (Version 2024):
[Poster](Poster_Backup&Restore_V2024.png)

## Lektionenplan

| Tag.Lekt | Themen       | Kom-peten-zen | Aufträge                                   |
|---------|------------|-------------|---------------------|
| 1.1 | **Einführung** Klasse und Modul <br> Thesen <br> [Übersicht Daten Rechenzentrum](01-Grundlagen/01-Einstieg/Data_Rechenzentrum.pdf)| - |  [01 PPT Einstieg](01-Grundlagen/01-Einstieg/01_Moduleinstieg_V1.5.pdf) <br> [02 Thesen - Auftrag](01-Grundlagen/02-Thesen/02_Moduleinstieg-These-Antithese.md)  |
| 1.2-4| Plenum, Think/Pair/Share: *Mögliche Use-Cases im Privatbereich und im Geschäft.* <br> **[Grundlagen](01-Grundlagen/README.md)**: <br> - Warum Datenschutz <br> [- Umfrage RA](01-Grundlagen/03-Weshalb-Datenschutz/Umfrage_23_Datenschutz.pdf) | A1, A2 |  [03 Theorie und Auftrag &rarr; MM](01-Grundlagen/03-Weshalb-Datenschutz/Weshalb_schuetze_ich_meine_Daten_v2.1.pdf) |
| | | | |
| 2.1-3 | - Repetition Datensicherheit <br> - Fachbegriffe | A1, B1, C1, A2 | Siehe auch [Poster](Poster_Backup_Restore_V2024.png) <br> [04 Arbeitsaufträge](01-Grundlagen/README.md) |
| 2.4 | - Speichermedien | B1 | [05 Arbeitsauftrag](https://gitlab.com/ch-tbz-it/Stud/m143/-/blob/main/01-Grundlagen/05-Speichermedien/README.md) |
| | _________________________________________ | | |
| 3.1 | - Repetition Aufträge & Speichermedien  | B1| Aufträge konsolidieren |
| 3.2-4 | Plenum: *Backup-Konzept in eigener Firma?* <br>Grossbrand in der Cloud <br>  [**Datensicherungskonzept**](02-Datensicherungskonzept/README.md) <br> - Einflussfaktoren <br> - Konzept-Anforderungen <br> - Konzept-Planung | A1, B1, C1 |  *Auftrag: Konzept eigene Firma &rarr; Diagramm* <br> <br> [01 Einflussfaktoren](02-Datensicherungskonzept/01-Einfuehrung/Einfuehrung_Datensicherungskonzept_inkl_Textfelder_v1.5.pdf) (Auftrag, Wissensaufbau kombiniert mit gemachten Erfahrungen aus der Praxis) <br> [02 Erhebung](02-Datensicherungskonzept/02-Erhebung/Backup-Konzept_On-Premise_v2.4.pdf) ([Erhebungsvorlage](02-Datensicherungskonzept/02-Planung/Backup-Konzept_On-Premise_Vorlage.xlsx) *herunterladen* und ausfüllen) <br> [03 Planung](02-Datensicherungskonzept/02-Planung/Datensicherung_v2.0.pdf) (Vorbereitung LB2-Projekt) |
| 4 | Plenum: *Backup-Konzept eigene Firma vorstellen* <br> - Konzept fertig planen (Backupart, Datenmenge, Wechsel-/Backupschema)  | A1, B1, C1 | [03 Planung](02-Datensicherungskonzept/02-Planung/Datensicherung_v2.0.pdf) (Vorbereitung LB2-Projekt) <br> Abgabe 03 Planung (Benotet 10%) |
| | _________________________________________ | | |
| 5.1 | **LB1 Schriftliche Lernkontrolle 30%** <br> Grundlagen | A1, A2, B1, C1 | Elektronisch oder Papier <br> Spick ausgedruckt, selbstgeschrieben|
| 5.2 - 7.4 | **LB2 Projekt "On-Premise-Backuplösung für heterogene IT-Umgebungen realisieren"** <br> Konzept <br> Infrastruktur & Umsetzung <br> Dokumentation | D1, D2, D3, E1, E2  | [Auftrag](04-LB2_On-Premise/) <br> VMs mit On-Premise Backup-Lösung |
| | | | |
| 8.1 - 10.1 <br> 10.2 - 10.4| **LB3 Machbarkeitsstudie: Cloud-Backup-Lösung** <br> Theorie <br> Auswahl Anbieter & Tool <br> Machbarkeitsstudie | D1, D2, D3, E1, E2| [Auftrag](05-LB3_Cloud/) <br> Cloud-Backup-Lösung evaluieren und Machbarkeit demonstrieren |


---

## Direkte Links:

- [**Grundlagen**](01-Grundlagen) _(Details)_
    - [Einstieg](01-Grundlagen/01-Einstieg)
    - [Thesen](01-Grundlagen/02-Thesen)
    - [Datenschutz](01-Grundlagen/03-Weshalb-Datenschutz)
    - [Fachbegriffe](01-Grundlagen/04-Fachbegriffe)
    - [Speichermedien](01-Grundlagen/05-Speichermedien)

---

- [**Datensicherungskonzept**](02-Datensicherungskonzept) _(Details)_
    - [Einführung](02-Datensicherungskonzept/01-Einfuehrung/Einfuehrung_Datensicherungskonzept_inkl_Textfelder_v1.5.pdf)
    - [Erhebung](02-Datensicherungskonzept/02-Erhebung/Backup-Konzept_Mittelbetrieb_inkl_Textfelder_v2.6.pdf)
    - [Planung (Benotet 10%)](02-Datensicherungskonzept/03-Planung)


---

- [**Leistungsbeurteilungen**](07-Leistungsbeurteilungen/README.md) _(Details)_
    - [**LB1** Schriftliche Lernkontrolle (30%)](07-Leistungsbeurteilungen/LB1)
    - [**LB2** On-Premise Backup-Infrastruktur (30%)](04-LB2_On-Premise/README.md)
    - [**LB3** Cloud Backup-Infrastruktur Proof-Of-Concept (30%)](05-LB3_Cloud/README.md)

---

- [**Ressourcen**](07-Ressourcen) _(Details)_ 
    - [Clips](07-Ressourcen/01-Clips)
    - [Links](07-Ressourcen/02-Links)  
    - [Literatur](07-Ressourcen/03-Literatur)
    - [Diverses](07-Ressourcen/04-Diverses)
    
---

[Teacher](https://gitlab.com/ch-tbz-it/TE/m143.git)



